1;

function quantified = quantification (signal, bit)
   max = 2^(bit-1);
   quantified = fix(max * signal);
endfunction

function [sig,rate] = sous_echantillionage (signal, step, sample)
    sig = signal(1:step:end);
    rate = fix(sample/step);
endfunction

function quantified = quantification_image(signal, n, depth)
  keep = depth - n;
  quantified = bitshift(signal,-keep);
  quantified = bitshift(quantified, keep);
endfunction

[signal,fs] = audioread('son2_qcm.wav');
info = audioinfo('son2_qcm.wav');
info

% Frequence 22050
% Quantification 16
% Canaux 1

for i = find(signal,10)
    signal(i)
end 

%  1.0e-04 *    
%    -0.3052
%    -0.3052
%    -0.3052
%    -0.3052
%    -0.3052
%    -0.3052
%    -0.3052
%    -0.3052
%    -0.6104
%    -0.6104

hold on
plot(0:1000,signal(400:1400))
scatter(0:1000,signal(400:1400),'xr')
hold off
figure

max = 2^(info.BitsPerSample-1);
signal_int = fix(max * signal);
signal_int(find(signal_int,10))

img = imread('crayons.jpg');
%image(img)
%imfinfo('photo.jpg')

%            Width: 900
%            Height: 595
%            BitDepth: 24 matlab / BitDepth : 8 Octave
%            NumberOfSamples: 3

%img(1,1:10,1:3)

s1 = quantification(signal,12);
%s2 = quantification(signal,8);
%s3 = quantification(signal,4);

%audiowrite('an_audio1.wav',s1,info.SampleRate);
%audiowrite('an_audio2.wav',s2,info.SampleRate);
%audiowrite('an_audio3.wav',s3,info.SampleRate);


[s1,e1] = sous_echantillionage(signal,2,info.SampleRate);
%[s2,e2] = sous_echantillionage(signal,4,info.SampleRate);

audiowrite('an_audio11.wav',quantification(s1,16),e1);
%audiowrite('an_audio21.wav',quantification(s2,16),e2);

info = imfinfo('crayons.jpg');
imgquant = quantification_image(img,2,info.BitDepth);
%imgquant(1,1:10,1:3)
image(imgquant);
imwrite(imgquant,'photo_out.jpg')
