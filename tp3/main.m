1;
function haut = my_pass_haut(fft,percent)
  haut = fft;
  L = min(size(fft));
  percent = 1 - percent;
  x2 = fix(sqrt((percent)*(L^2)));
  start = fix((L/2) - (x2/2));
  for i = 1:L
    for j = 1:L
      if ( i < start ) || ( i > L - start ) || (j < start ) || ( j > L - start )
        haut(i,j) = complex(0);
      endif
    endfor
  endfor
endfunction

function bas = my_pass_bas(fft,percent)
  bas = fft;
  L = min(size(fft));
  x1 = fix(sqrt(percent*(L^2)));
  start = fix((L/2) - (x1/2));
  bas(start:start+x1,start:start+x1) = complex(0);
endfunction

function disp2dfft(m_fft)
  figure();
  img = log(abs(m_fft));
  imagesc(img);
  colormap(gray(256));
endfunction

function img_out = filtre_median(img_in)
  img_out = img_in;
  L = min(size(img_in));
  for i = 2:L-1
     for j = 2:L-1
      t = img_out(i-1:i+1,j-1:j+1);
      t = reshape(t,1,[]);
      img_out(i,j) = median(t);
     endfor
  endfor
  
endfunction

function disp_my_img(img)
  figure();
  image(img);
  colormap(gray(256));
endfunction



%img = imread('lena.jpg');
%image(img);
%title('Image Lena origine');
%colormap(gray(256));
%fft_of_img = fft2(img);
%disp2dfft(fft_of_img);

% Les hautes frequences se trouvent sur le milieux 
% les basses frequences se trouvent sur les bords 
% 0.99
% 0.12

%imb = my_pass_haut(fft2(img),0.20);
%disp_my_img(abs(ifft2(imb)));
%img = imread('img_b.jpg');
%fft_of_img = fft2(img);
%bas = fft_of_img;
%bas = my_pass_bas(bas,0.80);
%bas_img = abs(ifft2(bas));
%disp_my_img(bas_img);
%debruite = filtre_median(img);
%disp_my_img(debruite)

img = imread('img.jpg');
fft_of_img = fft2(img);
fft_img_bord = my_pass_haut(fft_of_img,0.10);
disp2dfft(fft_img_bord);
img_bord = abs(ifft2(fft_img_bord));
disp_my_img(img_bord)






