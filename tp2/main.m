1;
function note = get_note_sec(a,freq,samplerate)
   unit = (1/samplerate);
   note = a*sin(2*(unit*(1:samplerate))*pi*freq);
end

function silence = get_silence_time(sample)
  silence(1:sample+1) = 0;
end

function quantified = quantification (signal, bit)
   max = 2^(bit-1);
   quantified = fix(max * signal);
endfunction

function bruite = get_bruit(n)
  bruite = (rand(1,n)*0.4)-0.2;  
end 

function mat_spectro = spectro(signal, taille_fenetre, pas)
  tab = [];
  for x = (1:pas:(length(signal)-taille_fenetre+1))
    tf = abs(fft(signal(x:x+taille_fenetre-1)));
    tab = [tab tf'(length(tf)/2:end)];
  endfor
  size(tab)
  mat_spectro = tab;
end

DO3 = get_note_sec(0.8,264,8000);
MI3 = get_note_sec(0.8,330,8000);
SOL3 = get_note_sec(0.8,396,8000);
DO4 = get_note_sec(0.8,528,8000);
Silence = get_silence_time(800);
S1 = [Silence DO3 Silence MI3 Silence SOL3 Silence DO4 Silence];
bruit = get_bruit(length(S1));
S2 = S1+bruit;

audiowrite("out1.wav",quantification(S1,16),8000);
audiowrite("out2.wav",quantification(S2,16),8000);

DO3 = get_note_sec(0.8,264,8000);
MI3 = get_note_sec(0.8,330,8000);
RE3 = get_note_sec(0.8,297,8000);

S3 = [ DO3 DO3 DO3 RE3 MI3 RE3 DO3 MI3 RE3 RE3 DO3 DO3 DO3 DO3 RE3 MI3 RE3 DO3 MI3 RE3 RE3 DO3];
audiowrite("out3.wav",quantification(S3,16),8000);
[S4,FS] = audioread('diner.wav');
S4 = S4';
%res = spectro(S4,1024,100);
%image(res)

do3 = get_note_sec(0.8,262,16000);
mi3 = get_note_sec(0.8,330,16000);
sol3 = get_note_sec(0.8,392,16000); 
la3 = get_note_sec(0.8,440,16000);
si3 = get_note_sec(0.8,494,16000);
do4 = get_note_sec(0.8,523,16000);
re4 = get_note_sec(0.8,587,16000);
silence = get_silence_time(1600);
S5 = [sol3 la3 si3 sol3 la3 silence la3 si3 do4 silence do4 si3 silence si3 sol3 la3 si3 sol3 la3 silence la3 si3 do4 re4 sol3 sol3];
melodie = quantification(S5,16);
audiowrite("melodie.wav",melodie,16000);


res = spectro(S5,1024,100);
image(res)

%res = spectro(S2,1024,100);
%image(res)
%figure()
%res = spectro(S1,1024,100);
%image(res)
%figure()
%res = spectro(S1,1024,10);
%image(res)
%figure()
%res = spectro(S1,1024,1000);
%image(res)
%figure()
%res = spectro(S1,2048,100);
%image(res)
%figure()
%res = spectro(S1,512,100);
%image(res)
%figure()