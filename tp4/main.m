1;
function [x,y] = hermite(step,p0,p1,t0,t1)
  courbe = [];
  for ind = (0:step:1)
    h0 = ((ind-1)**2)*(2*ind+1);
    h1 = (ind**2)*(3-2*ind);
    h2 = ind*(ind-1)**2;
    h3 = (ind**2)*(ind-1);
    r = h0*p0 + h1*p1 + h2*t0 + h3*t1;
    courbe = [courbe, r];
  endfor
  courbe = reshape(courbe,2,[]);
  x = courbe(1,:);
  y = courbe(2,:);
endfunction

function C = bezier(L,pas)
    n = max(size(L))-1;
    C = zeros(2,fix(1/pas)+1);
    t = 1;
    for u = (0:pas:1)
      for k = (0:n)
        B = nchoosek(n,k)*(u^k)*(1-u)^(n - k);
        C(1,t) = C(1,t) + B * L(1,k+1);
        C(2,t) = C(2,t) + B * L(2,k+1);
      endfor
      t = t + 1;
     endfor
endfunction 

function multi_bezier(L,pas)
  hold on 
  for index = (1:2:size(L)(1))  
    compo = L(index:index+1,:);
    datax = [];
    datay = [];
    for index_2 = (1:size(L)(2))
      pt = compo(:,index_2);
      if pt(1) != 0 && pt(2) != 0 
        datax = [datax pt(1)];
        datay = [datay pt(2)];
      endif
    endfor
    point = bezier([datax;datay],pas);
    plot(point(1,:),point(2,:),'b')
  endfor
endfunction 



p0 = [50,50];
p1 = [150,50];
t0 = [-180,400];
t1 = [-5,100];
[x,y] = hermite(0.01,p0,p1,t0,t1);
%plot(x,y);
m = [2 1 4 4 5 6 6 9 8;3 2 1 7 9 7 1 2 3;2 1 9 8 0 0 0 0 0; 5 4 5 4 0 0 0 0 0];
multi_bezier(m,0.025)